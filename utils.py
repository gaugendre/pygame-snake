import random
from enum import Enum

import pygame

from config import FONT_COLOR, RESOLUTION, MAP_SIZE, TILE_SIZE


def get_score_text(score):
    font = pygame.font.Font(None, 30)
    text = font.render(f"Score : {score}", 1, FONT_COLOR)
    text_rect = text.get_rect()  # type: pygame.Rect
    text_rect.left = 0
    text_rect.bottom = RESOLUTION[1]
    return text, text_rect


def make_slot(left: int, top: int) -> pygame.Rect:
    if left >= MAP_SIZE[0] or left < 0:
        raise ValueError(f'left must be between 0 and {MAP_SIZE[0]}')
    if top >= MAP_SIZE[1] or top < 0:
        raise ValueError(f'top must be between 0 and {MAP_SIZE[1]}')

    return pygame.Rect(left * TILE_SIZE, top * TILE_SIZE, TILE_SIZE, TILE_SIZE)


def random_slot() -> pygame.Rect:
    return make_slot(random.randint(0, MAP_SIZE[0] - 1), random.randint(0, MAP_SIZE[1] - 1))


class Direction(Enum):
    UP = 0, -TILE_SIZE
    RIGHT = TILE_SIZE, 0
    DOWN = 0, TILE_SIZE
    LEFT = -TILE_SIZE, 0

    @staticmethod
    def are_opposed(d1, d2):
        return (d1 == Direction.UP and d2 == Direction.DOWN
                or d1 == Direction.DOWN and d2 == Direction.UP
                or d1 == Direction.LEFT and d2 == Direction.RIGHT
                or d1 == Direction.RIGHT and d2 == Direction.LEFT)

    def is_opposed_to(self, other):
        return Direction.are_opposed(self, other)
