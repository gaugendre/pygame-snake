BACKGROUND_COLOR = 0, 0, 0
APPLE_COLOR = 250, 0, 0
SNAKE_COLOR = 0, 250, 0
FONT_COLOR = 250, 250, 250

MAP_SIZE = 41, 31
TILE_SIZE = 20
MAP_RESOLUTION = MAP_SIZE[0] * TILE_SIZE, MAP_SIZE[1] * TILE_SIZE
RESOLUTION = MAP_RESOLUTION[0], MAP_RESOLUTION[1] + 50

INITIAL_SNAKE_SIZE = 3

MAX_HIGH_SCORES_COUNT = 10

SCORES_FILE = 'scores.snake'
